
### Introduction

This is the consumer which is used with laravel_boilerplate_with_passport-oauth2.0 which works as a provider in this service.
you can find the laravel_boilerplate_with_passport-oauth2.0 at git clone https://FxDataLabs@bitbucket.org/FxDataLabs/laravel_boilerplate_with_passport-oauth2.0.git and follow the below steps to use Laravel Boilerplate with Passport(OAuth2.0).

### How to use

- git clone https://FxDataLabs@bitbucket.org/FxDataLabs/laravel_boilerplate_with_passport-oauth2.0.git

- add you website name in /etc/hosts and homestead.yml file

- vagrant up

- <yourboilerplatewebsitename> in browser

- Login in website and create a client from (Create New Client)

- give any name and add http://<yourconsumerwebsitename>/callback in redirect url

(This is simple Laravel Boilerplate with socialite login and email verification)


- git clone https://FxDataLabs@bitbucket.org/FxDataLabs/consumer_for_oauth.git

- add you website name in /etc/hosts and homestead.yml file.

- vagrant up

- add <yourboilerplatewebsitename> in web.php in consumer project.

- copy client_id and client_secret from <yourboilerplatewebsitename> and paste

- <yourconsumerwebsitename> in browser 

(This consumer is used for Passport funcationlities where laravel_boilerplate_with_passport-oauth2.0 serve as a provider)

### Contributing

Please feel free to make any pull requests, or e-mail me a feature request you would like to see in the future to FxDataLabs @ contact@htree.plus
