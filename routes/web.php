<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use GuzzleHttp\Client;

// First route that user visits on consumer app
Route::get('/', function () {
    // Build the query parameter string to pass auth information to our request
    $query = http_build_query([
        'client_id' => 4,
        'redirect_uri' => 'http://yourconsumerwebsitename/callback',
        'response_type' => 'code',
        'scope' => 'conference'
    ]);

    // Redirect the user to the OAuth authorization page
    return redirect('http://yourboilerplatewebsitename/oauth/authorize?' . $query);
});

// Route that user is forwarded back to after approving on server
Route::get('callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://yourboilerplatewebsitename/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 4, 
            'client_secret' => '1yDDr925azglYMhO0nHQwcXoAXAtVAyJh5jlQH98', 
            'redirect_uri' => 'http://yourconsumerwebsitename/callback',
            'code' => $request->code // Get code from the callback
        ]
    ]);

    // echo the access token; normally we would save this in the DB
    return json_decode((string) $response->getBody(), true)['access_token'];
});